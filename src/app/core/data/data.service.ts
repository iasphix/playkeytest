import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {API_PATH} from '../../../settings';
import {catchError} from 'rxjs/operators';
import {from} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private $httpClient: HttpClient) {
  }

  get(path, request = {}) {
    return this.$httpClient.get(`${API_PATH + path}`, request).pipe(
      catchError(err => {
        console.error(`Got ${err.status}: ${err.description}`);
        return from(['']);
      })
    );
  }
}

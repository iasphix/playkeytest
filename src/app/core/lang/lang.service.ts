import {Injectable} from '@angular/core';
import {DataService} from '../data/data.service';
import {BehaviorSubject, Subscriber} from 'rxjs';
import {ActivatedRouteSnapshot, CanActivate} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LangService {
  langDictionary;
  $dictionary: Subscriber<any>;
  currentLanguage: BehaviorSubject<string> = new BehaviorSubject('ru');

  constructor(private dataService: DataService) {
    this.setDictionary(this.getDictionary(this.currentLanguage.getValue()));
  }

  switchLang(lang) {
    this.currentLanguage.next(lang);
    return this.setDictionary(this.getDictionary(this.currentLanguage.getValue()));
  }

  getDictionary(lang) {
    return this.dataService.get(`i18n.json`);
  }

  setDictionary($dictionary) {
    return this.$dictionary = $dictionary.subscribe(dictionary => {
      this.langDictionary = dictionary[this.currentLanguage.getValue()];
      this.$dictionary.unsubscribe();
    });
  }

  getText(text) {
    if (this.langDictionary) {
      return this.langDictionary[text];
    }
  }

}

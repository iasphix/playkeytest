import {Component, OnInit} from '@angular/core';
import {LangService} from '../lang.service';
import {LangComponent} from '../lang.component';

@Component({
  selector: 'app-switch-lang',
  templateUrl: './switch-lang.component.html',
  styleUrls: ['./switch-lang.component.scss']
})
export class SwitchLangComponent extends LangComponent {

  constructor(langService: LangService) {
    super(langService);
  }
  
}

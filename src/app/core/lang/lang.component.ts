import {Component, OnInit} from '@angular/core';
import {LangService} from './lang.service';

@Component({
  selector: 'app-lang',
  template: ''
})
export class LangComponent {

  constructor(private langService: LangService) {
  }

  switchLang(lang) {
    return this.langService.switchLang(lang);
  }

  getText(text) {
    return this.langService.getText(text);
  }
}

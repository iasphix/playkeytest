import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() {
  }

  addToStorage(key: string, data: string) {
    return localStorage.setItem(key, data);
  }

  getFromStorage(key: string) {
    return localStorage.getItem(key);
  }

  removeFromStorage(key: string) {
    return localStorage.removeItem(key);
  }

}

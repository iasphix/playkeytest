import {Injectable} from '@angular/core';
import {LangService} from '../lang/lang.service';


@Injectable({
  providedIn: 'root'
})
export class PriceService {

  constructor(private langService: LangService) {
  }

  getPrice(price) {
      switch (this.langService.currentLanguage.getValue()) {
        case 'ru': {
          return this.getRublePrice(price.rub);
        }
        case 'en': {
          return this.getPoundPrice(price.pound);
        }
        default:
      }
  }

  getPoundPrice(price) {
    return (price / 100).toFixed(2);
  }

  getRublePrice(price) {
    return Math.floor(price / 100);
  }

  getTotalPrice(prices) {
    let totalPrice = 0;
    prices.forEach(price => {
      if (price) {
        totalPrice = +totalPrice + +this.getPrice(price);
      }
    });
    return totalPrice;
  }

}

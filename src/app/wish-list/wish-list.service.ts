import {Injectable} from '@angular/core';
import {StorageService} from '../core/storage/storage.service';
import {BehaviorSubject, Subscriber, Subscription} from 'rxjs';
import {ProductsService} from '../products/products.service';
import {ProductsListItemModel} from '../products/products.model';
import {PriceService} from '../core/price/price.service';

@Injectable({
  providedIn: 'root'
})
export class WishListService {
  wishedProducts$: BehaviorSubject<any> = new BehaviorSubject([]);
  subscription$: Subscription;

  constructor(private storageService: StorageService,
              private productService: ProductsService,
              private priceService: PriceService) {
    const storageData = storageService.getFromStorage('wishList');
    if (storageData) {
      this.getWishProductsFromLocalStorage(storageData);

      this.subscription$ = this.productService.currentPageProducts$.subscribe(products => {
        if (this.subscription$ && products) {
          this.subscription$.unsubscribe();
          return this.productService.setBaseWishedProductsStatus(this.wishedProducts$.getValue());
        }
      });
    }
  }

  addProductToWishList(product) {
    if (this.wishedProducts$.getValue().some(wishedProduct => wishedProduct.id === product.id)) {
      return;
    }
    this.wishedProducts$.next(this.wishedProducts$.getValue().concat(product));
    this.addWishProductsToLocalStorage();
    return this.productService.changeWishedProductStatus(product.id, true);
  }

  deleteProductFromWishList(product) {
    this.wishedProducts$.next(this.wishedProducts$.getValue().filter(wishedProduct => wishedProduct.id !== product.id));
    this.addWishProductsToLocalStorage();
    return this.productService.changeWishedProductStatus(product.id, false);
  }

  clearWishList() {
    this.wishedProducts$.next([]);
    this.addWishProductsToLocalStorage();
    this.productService.currentPageProducts$.getValue().map(((product: ProductsListItemModel) => product.isWished = false));
  }

  addWishProductsToLocalStorage() {
    return this.storageService.addToStorage('wishList', JSON.stringify(this.wishedProducts$.getValue()));
  }

  getWishProductsFromLocalStorage(jsonStr) {
    this.wishedProducts$.next(JSON.parse(jsonStr));
  }

  getWishListItemsTotalPrice() {
    const prices = this.wishedProducts$.getValue().map((wishedProduct: ProductsListItemModel) => {
      return wishedProduct.price;
    });
    return this.priceService.getTotalPrice(prices);
  }

}

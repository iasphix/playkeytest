import {Component, OnInit} from '@angular/core';
import {WishListService} from './wish-list.service';
import {ProductsService} from '../products/products.service';
import {Observable} from 'rxjs';
import {LangService} from '../core/lang/lang.service';
import {LangComponent} from '../core/lang/lang.component';
import {ProductsListItemModel} from '../products/products.model';

@Component({
  selector: 'app-wish-list',
  templateUrl: './wish-list.component.html',
  styleUrls: ['./wish-list.component.scss']
})
export class WishListComponent extends LangComponent implements OnInit {
  $wishListItems: Observable<ProductsListItemModel[]>;
  isDraggableItem: boolean;

  constructor(
    langService: LangService,
    private wishListService: WishListService,
    private productsService: ProductsService,
  ) {
    super(langService);
  }

  ngOnInit() {
    this.$wishListItems = this.wishListService.wishedProducts$;
  }

  getWishListItemsTotalPrice() {
    return this.wishListService.getWishListItemsTotalPrice();
  }

  clearWistListItems() {
    this.wishListService.clearWishList();
  }

  deleteFromWishList(product: ProductsListItemModel) {
    return this.wishListService.deleteProductFromWishList(product);
  }

  dragOver(event) {
    this.isDraggableItem = true;
    event.preventDefault();
  }

  dragLeave(event) {
    this.isDraggableItem = false;
    event.preventDefault();
  }

  onDrop(event) {
    this.wishListService.addProductToWishList(this.productsService._draggableProduct$.getValue());
    this.isDraggableItem = false;
  }

  dragEnter(event) {
    event.preventDefault();
    return true;
  }

}

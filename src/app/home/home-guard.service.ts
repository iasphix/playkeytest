import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate} from '@angular/router';
import {LangService} from '../core/lang/lang.service';

@Injectable({
  providedIn: 'root'
})
export class HomeGuardService implements CanActivate{

  constructor(private langService: LangService) { }

  canActivate(route: ActivatedRouteSnapshot) {
    const langParam = route.params['lang'];
    if (langParam) {
      this.langService.switchLang(langParam);
    }
    return true;
  }
}

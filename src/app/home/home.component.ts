import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {LangService} from '../core/lang/lang.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private route: ActivatedRoute, private langService: LangService) { }

  ngOnInit() {
  }

}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ProductsListItemComponent } from './products/products-list-item/products-list-item.component';
import {ProductsListComponent} from './products/products-list/products-list.component';
import {HttpClientModule} from '@angular/common/http';
import {LangService} from './core/lang/lang.service';
import {DataService} from './core/data/data.service';
import {ProductsService} from './products/products.service';
import {WishListComponent} from './wish-list/wish-list.component';
import { SwitchLangComponent } from './core/lang/switch-lang/switch-lang.component';
import {AppRoutingModule} from './app-routing.module';
import {RouterModule} from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LangComponent } from './core/lang/lang.component';

@NgModule({
  declarations: [
    LangComponent,
    AppComponent,
    ProductsListComponent,
    ProductsListItemComponent,
    WishListComponent,
    SwitchLangComponent,
    HomeComponent,
  ],
  imports: [
    RouterModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [ProductsService, LangService, DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }

import {Component, Input} from '@angular/core';
import {ProductsListItemModel} from '../products.model';
import {LangService} from '../../core/lang/lang.service';
import {PriceService} from '../../core/price/price.service';
import {WishListService} from '../../wish-list/wish-list.service';
import {ProductsService} from '../products.service';
import {LangComponent} from '../../core/lang/lang.component';

@Component({
  selector: 'app-products-list-item',
  templateUrl: './products-list-item.component.html',
  styleUrls: ['./products-list-item.component.scss'],
})

export class ProductsListItemComponent extends LangComponent {
  @Input() product: ProductsListItemModel;
  isDraggable: boolean;

  constructor(
    langService: LangService,
    private wishListService: WishListService,
    private priceService: PriceService,
    private productsService: ProductsService
  ) {
    super(langService);
  }

  getItemProductButtonText(): string {
    if (this.product.isWished) {
      return this.getText('added');
    } else {
      return this.getText('add');
    }
  }

  getPrice(price) {
    if (price) {
      return this.priceService.getPrice(price);
    }
  }

  changeProductWishStatus(product: ProductsListItemModel) {
    if (product.isWished) {
      this.wishListService.deleteProductFromWishList(product);
    } else {
      this.wishListService.addProductToWishList(product);
    }
  }

  onDragStart(event, draggableProduct: ProductsListItemModel) {
    if (draggableProduct.isWished) {
      event.preventDefault();
      return false;
    }
    event.dataTransfer.setData('text', 'anything');
    this.productsService._draggableProduct$.next(draggableProduct);
    this.isDraggable = true;
  }

  onDragEnd(event: Event) {
    this.isDraggable = false;
  }

}

import {Injectable} from '@angular/core';
import {ProductsListItemModel} from './products.model';
import {BehaviorSubject} from 'rxjs';
import {map} from 'rxjs/operators';
import {DataService} from '../core/data/data.service';

@Injectable({
  providedIn: 'root'
})

export class ProductsService {
  currentPageProducts$: BehaviorSubject<ProductsListItemModel[]> = new BehaviorSubject(null);
  _draggableProduct$: BehaviorSubject<ProductsListItemModel> = new BehaviorSubject(null);

  constructor(private dataService: DataService) {
    this.getProducts().subscribe(products => {
      this.currentPageProducts$.next(products);
    });
  }

  getProducts(): any {
    return this.dataService.get(`products.json`).pipe(
      map((response: ProductsListItemModel[]) => {
        if (response) {
          response.map((item, index) => {
            item.id = index;
          });
          return response;
        }
      })
    );
  }

  setBaseWishedProductsStatus(wishedProducts: ProductsListItemModel[]) {
    const nextCurrentPageProducts = this.currentPageProducts$.getValue()
      .map(product => {
        if (wishedProducts.some(wishedProduct => wishedProduct.id === product.id)) {
          product.isWished = true;
        }
        return product;
      });
    return this.currentPageProducts$.next(nextCurrentPageProducts);
  }

  changeWishedProductStatus(productId: number, newStatus: boolean) {
    const nextProducts = this.currentPageProducts$.getValue()
      .map((product: ProductsListItemModel) => {
      if (product.id === productId) {
        product.isWished = newStatus;
      }
      return product;
    });

    return this.currentPageProducts$.next(nextProducts);
  }

}



import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {ProductsListItemModel} from '../products.model';
import {ProductsService} from '../products.service';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss']
})
export class ProductsListComponent implements OnInit {
  $products: Observable<ProductsListItemModel[]>;

  constructor(private productsService: ProductsService) {
  }

  ngOnInit() {
    this.$products = this.productsService.currentPageProducts$;
  }
}

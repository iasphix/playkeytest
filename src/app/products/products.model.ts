export interface ProductsListItemModel {
  id: number;
  name: string;
  price: {
    rub: number,
    pound: number
  };
  cover: string;
  isWished?: any;
}
